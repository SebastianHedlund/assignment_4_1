package calculator;

public class Calculator {

    public int add(int numberOne, int numberTwo){
        return numberOne + numberTwo;
    }

    public int subtract(int numberOne, int numberTwo){
        return numberOne - numberTwo;
    }

    public int multiply(int numberOne, int numberTwo){
        return numberOne * numberTwo;
    }

    public int divide(int numberOne, int numberTwo) throws DivisionByZeroException {
        if (numberTwo == 0)
            throw new DivisionByZeroException();
        return numberOne / numberTwo;
    }
}
