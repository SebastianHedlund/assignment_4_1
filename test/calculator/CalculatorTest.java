package calculator;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    public void calculateAddition_NumberOnePlusNumberTwo_ReturnSum(){
        //Arrange
        int numberOne = 2;
        int numberTwo = 3;
        int expected = 5;
        Calculator calculator = new Calculator();

        //Act
        int actual = calculator.add(numberOne, numberTwo);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void calculateSubtraction_NumberOneMinusNumberTwo_ReturnDifference(){
        //Arrange
        int numberOne = 2;
        int numberTwo = 3;
        int expected = -1;
        Calculator calculator = new Calculator();

        //Act
        int actual = calculator.subtract(numberOne, numberTwo);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void calculateMultiplication_NumberOneTimesNumberTwo_ReturnProduct(){
        //Arrange
        int numberOne = 2;
        int numberTwo = 3;
        int expected = 6;
        Calculator calculator = new Calculator();

        //Act
        int actual = calculator.multiply(numberOne, numberTwo);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void calculateDivision_NumberOneDividedByNumberTwo_ReturnQuotient() throws DivisionByZeroException {
        //Arrange
        int numberOne = 6;
        int numberTwo = 3;
        int expected = 2;
        Calculator calculator = new Calculator();

        //Act
        int actual = calculator.divide(numberOne, numberTwo);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void calculateDivision_NumberTwoEqualsZero_ThrowsException(){
        //Arrange
        int numberOne = 4;
        int numberTwo = 0;
        Calculator calculator = new Calculator();

        //act and assert
        assertThrows(DivisionByZeroException.class, () -> calculator.divide(numberOne, numberTwo));

    }

}